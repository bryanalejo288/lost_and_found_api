<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// Establish a database connection (modify with your database credentials)
$conn = new mysqli("localhost", "root", "", "flutter_auth");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Perform a database query to fetch items
$sql = "SELECT * FROM items";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $items = array();
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }
    echo json_encode(array("items" => $items));
} else {
    echo json_encode(array("message" => "No items found"));
}

// Close the database connection
$conn->close();
?>
