<?php
$db = mysqli_connect('localhost', 'root', '', 'flutter_auth');
if (!$db) {
    echo json_encode(array("status" => "Error", "message" => "Database Connection Failed"));
    exit; // Terminate the script
}

$username = $_POST["username"];
$password = $_POST["password"];

// Use prepared statement to retrieve the user by username
$stmt = $db->prepare("SELECT * FROM users WHERE username = ?");
$stmt->bind_param("s", $username);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows == 1) {
    $user = $result->fetch_assoc();
    $hashedPassword = $user['password'];

    // Verify the entered password against the stored hash
    if (password_verify($password, $hashedPassword)) {
        echo json_encode(array("status" => "Success", "message" => "Login Success"));
    } else {
        echo json_encode(array("status" => "Error", "message" => "Incorrect Password"));
    }
} else {
    echo json_encode(array("status" => "Error", "message" => "User Not Found"));
}

// Close the database connection
mysqli_close($db);
?>
