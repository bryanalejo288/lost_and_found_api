<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// Check if the request method is POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $uploadDir = 'C:/xampp/htdocs/api_lost_and_found/uploads/';
    // Read the JSON data from the request body
    $data = json_decode(file_get_contents("php://input"));

    if (!file_exists($uploadDir)) {
        mkdir($uploadDir, 0777, true);
    }
    
    // Check if the 'image' field is set in the POST request
    if (isset($_FILES['image'])) {
        $file = $_FILES['image'];
        
        // Get the original filename and create a unique name for the uploaded file
        $originalFileName = $file['name'];
        $fileExtension = pathinfo($originalFileName, PATHINFO_EXTENSION);
        $uniqueFileName = uniqid() . '.' . $fileExtension;
        
        // Move the uploaded file to the target directory
        $targetPath = $uploadDir . $uniqueFileName;
        move_uploaded_file($file['tmp_name'], $targetPath);
        
        // Handle other form fields (title, description, location, dates, contact_email)
        $title = $_POST['title'];
        $description = $_POST['description'];
        $location = $_POST['location'];
        $dateLost = $_POST['date_lost'];
        $dateFound = $_POST['date_found'];
        $contactEmail = $_POST['contact_email'];
        
        // Insert the data into your database (replace with your database code)
        $dbHost = 'localhost';
        $dbUser = 'root';
        $dbPass = '';
        $dbName = 'flutter_auth';
        
        $conn = new mysqli($dbHost, $dbUser, $dbPass, $dbName);
        
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        
        // Insert the data into your database table
        $sql = "INSERT INTO items (title, description, location, date_lost, date_found, contact_email, image_path)
                VALUES ('$title', '$description', '$location', '$dateLost', '$dateFound', '$contactEmail', '$targetPath')";
        
        if ($conn->query($sql) === TRUE) {
            echo "Item created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        
        $conn->close();
    } else {
        echo "No image file uploaded.";
    }
} else {
    echo "Invalid request method. POST requests are required.";
}
?>